<div class="banner">
  <div class="container">
    <div class="banner_info">
      <h3>Millions of verified Members</h3>
      <h4>Register <i style="margin:0 20px; font-size:40px;" class="fa fa-play"></i> Connect <i style="margin:0 20px; font-size:40px;" class="fa fa-play"></i> Interact</h4>
      <a href="../users/register" class="hvr-shutter-out-horizontal">Create your Profile</a>
    </div>
  </div>
</div>
