<div class="grid_3">
  <div class="container">
    <div class="breadcrumb1">
      <ul>
        <a href="register"><i class="fa fa-home home_1"></i></a> <span class="divider">&nbsp;|&nbsp;</span>
        <li class="current-page">Register</li>
      </ul>
    </div>
    <div class="services">
      <div class="col-md-8 col-sm-12 login_left">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active" ><a href="#tab_content1" id="" role="tab" data-toggle="tab" aria-expanded="true" >USERS INFO</a> </li>
            <li role="presentation" ><a href="#tab_content2" id="" role="tab" data-toggle="tab" aria-expanded="true" >BASIC</a> </li>
            <li role="presentation" class="" ><a href="#tab_content2" role="tab" id="" data-toggle="tab" aria-expanded="false" >LOCATION</a> </li>
            <li role="presentation" class="" ><a href="#tab_content4" role="tab" id="" data-toggle="tab" aria-expanded="false" >EDUCATION CAREER</a> </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
              <form>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="actionBar"> <a href="#" class="btn btn-default" >Cancel</a> <a action="#" class="btnNext btn btn-success" >Next</a> </div>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
              <form>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="actionBar"> <a href="#" class="btn btn-default" >Cancel</a>  <a href="#" class="btnPrevious  btn btn-primary">Previous</a> <a action="#" class="btnNext btn btn-success" >Next</a></div>
              </form>
             
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
             <form>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="actionBar"> <a href="#" class="btn btn-default" >Cancel</a>  <a href="#" class="btnPrevious  btn btn-primary">Previous</a> <a action="#" class="btnNext btn btn-success" >Next</a></div>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
             <form>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label for="">Username <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="" name="pass" size="60" maxlength="128" class="form-text required">
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" id="" name="name" value="" size="60" maxlength="60" class="form-text required">
                  </div>
                  <div class="age_select">
                    <label for="edit-pass">Age <span class="form-required" title="This field is required.">*</span></label>
                    <div class="age_grid">
                      <div class="col-sm-4 form_box">
                        <div class="select-block1">
                          <select>
                            <option value="">Date</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box2">
                        <div class="select-block1">
                          <select>
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4 form_box1">
                        <div class="select-block1">
                          <select>
                            <option value="">Year</option>
                            <option value="">1980</option>
                            <option value="">1981</option>
                            <option value="">1982</option>
                            <option value="">1983</option>
                            <option value="">1984</option>
                            <option value="">1985</option>
                            <option value="">1986</option>
                            <option value="">1987</option>
                            <option value="">1988</option>
                            <option value="">1989</option>
                            <option value="">1990</option>
                            <option value="">1991</option>
                            <option value="">1992</option>
                            <option value="">1993</option>
                            <option value="">1994</option>
                            <option value="">1995</option>
                            <option value="">1996</option>
                            <option value="">1997</option>
                            <option value="">1998</option>
                            <option value="">1999</option>
                            <option value="">2000</option>
                            <option value="">2001</option>
                            <option value="">2002</option>
                            <option value="">2003</option>
                            <option value="">2004</option>
                            <option value="">2005</option>
                            <option value="">2006</option>
                            <option value="">2007</option>
                            <option value="">2008</option>
                            <option value="">2009</option>
                            <option value="">2010</option>
                            <option value="">2011</option>
                            <option value="">2012</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"> </div>
                    </div>
                  </div>
                  <div class="form-group form-group1">
                    <label class="col-sm-7 control-lable" for="sex">Sex : </label>
                    <div class="col-sm-5">
                      <div class="radios">
                        <label for="radio-01" class="label_radio">
                          <input type="radio" checked="">
                          Male </label>
                        <label for="radio-02" class="label_radio">
                          <input type="radio">
                          Female </label>
                      </div>
                    </div>
                    <div class="clearfix"> </div>
                  </div>
                  <div class="form-group">
                    <label for="edit-name">Subject <span class="form-required" title="This field is required.">*</span></label>
                    <textarea class="form-control bio" placeholder="" rows="3"></textarea>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="actionBar"> <a href="#" class="btn btn-default" >Cancel</a> <a href="#" class="btnPrevious  btn btn-primary">Previous</a> <a action="#" class=" btn btn-success" >Submit</a> </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-12 col-xs-12 col-md-offset-1">
      <h2> Social Signup</h2><hr>
        <ul class="sharing">
          <li><a href="#" class="facebook" title="Facebook"><i class="fa fa-boxed fa-fw fa-facebook"></i> Share on Facebook</a></li>
          <li><a href="#" class="twitter" title="Twitter"><i class="fa fa-boxed fa-fw fa-twitter"></i> Tweet</a></li>
          <li><a href="#" class="google" title="Google"><i class="fa fa-boxed fa-fw fa-google-plus"></i> Share on Google+</a></li>
          <li><a href="#" class="linkedin" title="Linkedin"><i class="fa fa-boxed fa-fw fa-linkedin"></i> Share on LinkedIn</a></li>
          <li><a href="#" class="mail" title="Email"><i class="fa fa-boxed fa-fw fa-envelope-o"></i> E-mail</a></li>
        </ul>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>
<div class="map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3150859.767904157!2d-96.62081048651531!3d39.536794757966845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1408111832978"> </iframe>
</div>
<div class="footer">
  <div class="container">
    <div class="col-md-4 col_2">
      <h4>About Us</h4>
      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
    </div>
    <div class="col-md-2 col_2">
      <h4>Help & Support</h4>
      <ul class="footer_links">
        <li><a href="#">24x7 Live help</a></li>
        <li><a href="contact.html">Contact us</a></li>
        <li><a href="#">Feedback</a></li>
        <li><a href="faq.html">FAQs</a></li>
      </ul>
    </div>
    <div class="col-md-2 col_2">
      <h4>Quick Links</h4>
      <ul class="footer_links">
        <li><a href="privacy.html">Privacy Policy</a></li>
        <li><a href="terms.html">Terms and Conditions</a></li>
        <li><a href="services.html">Services</a></li>
      </ul>
    </div>
    <div class="col-md-2 col_2">
      <h4>Social</h4>
      <ul class="footer_social">
        <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
        <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
        <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
        <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
      </ul>
    </div>
    <div class="clearfix"> </div>
    <div class="copy">
      <p>Copyright © 2015 Marital . All Rights Reserved   </p>
    </div>
  </div>
</div>
<script>
  $('#btnNext').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('#btnPrevious').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
</script>
</body>

</html>
