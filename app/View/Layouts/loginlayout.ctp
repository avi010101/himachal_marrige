<!DOCTYPE HTML>
<html>
    <head>
        <title>Himachalishadi.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="/himachal_marrige/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's Java Script plug in) -->
        <!-- Optional, Add fancyBox for media, buttons, thumbs -->
<!--        <link rel="stylesheet" href="/himachal_marrige/fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
        <script type="text/javascript" src="/himachal_marrige/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
        <script type="text/javascript" src="/himachal_marrige/fancybox/source/helpers/jquery.fancybox-media.js"></script>
        <link rel="stylesheet" href="/himachal_marrige/fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
        <script type="text/javascript" src="/himachal_marrige/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script> Optional, Add mousewheel effect 
        <script type="text/javascript" src="/himachal_marrige/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>-->
        <script src="/himachal_marrige/js/jquery.min.js"></script>
        <script src="/himachal_marrige/js/bootstrap.min.js"></script>
        <link href="/himachal_marrige/css/bootstrap-datepicker3.min.css" rel='stylesheet' type='text/css' />
        <link href="/himachal_marrige/css/style.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="/himachal_marrige/css/font-awesome.css" rel="stylesheet"> 
        <link href="/himachal_marrige/css/flexslider.css" rel='stylesheet' type='text/css' />
        <script src="/himachal_marrige/js/jquery.flexisel.js"></script>
        <!-- Custom Theme files -->
        <link href="/himachal_marrige/css/custom.css" rel='stylesheet' type='text/css' />
        <script>
            $(document).ready(function () {
                $(".dropdown").hover(
                        function () {
                            $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                            $(this).toggleClass('open');
                        },
                        function () {
                            $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                            $(this).toggleClass('open');
                        }
                );
            });
        </script>
    </head>
    <body>
        <?php echo $this->element('loginheader'); ?>
        <?php echo $this->fetch('content'); ?>
        <script src="/himachal_marrige/js/bootstrap-datepicker.min.js"></script>
    </body>
</html>