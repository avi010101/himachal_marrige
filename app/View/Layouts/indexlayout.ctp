<!DOCTYPE HTML>
<html>
<head>
<title>Himachalishadi.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="../css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<link href="../css/bootstrap-datepicker3.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


<link href="../css/style.css" rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link href="../css/font-awesome.css" rel="stylesheet"> 
<link href="../css/popup.css" rel='stylesheet' type='text/css' />
<link href="../css/flexslider.css" rel='stylesheet' type='text/css' />
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<link href="../css/custom.css" rel='stylesheet' type='text/css' />


</head>
<body>
  
     <?php echo $this->element('home_header'); ?>
     <?php echo $this->element('banner'); ?>
     <?php echo $this->element('profilesearch'); ?><br>
     <?php //echo $this->element('grid1'); ?>
     <?php echo $this->element('grid2'); ?>
     <?php echo $this->element('bg'); ?>
     <?php echo $this->element('map'); ?>
     <?php echo $this->element('footer'); ?>

<script src="../js/jquery.flexisel.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/jquery.magnific-popup.js"></script>
<script src="../js/bootstrap-datepicker.min.js"></script>
</body>
</html> 