<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User'); ?>
<style type="text/css">
    .form-required{
        color: #C32143;
    }
    #flashMessage.error {

        background: #C32143 url("../images/error.png") no-repeat;
        border-color: black;
        color: #FFFFFF;
        padding:5px 25px;
        width: 555px;
    }
    #flashMessage.success {

        background: #C32143 url("../images/error.png") no-repeat;
        border-color: black;
        color: #FFFFFF;
        padding:5px 25px;
        width: 555px;
    }
</style>

<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="../homes/index"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">User Login  (login with email & password)</li>
            </ul>
        </div>
        <div id='flashMessages'>
            <?php echo $this->Flash->render() ?> 

        </div>
        <div class="services">
            <div class="col-sm-6 login_left">
                <form action="login" method="post" id="form">
                    <div class="form-item form-type-textfield form-item-name">
                        <label for="edit-name">E-mail <span class="form-required" title="This field is required.">*</span></label>
                        <input type="text" id="email" name="User[email]" value="" size="60" maxlength="60" class="form-text required" >
                    </div>
                    <div class="form-item form-type-password form-item-pass">
                        <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                        <input type="password" id="password" name="User[password]" class="form-text required"  >
                    </div><br><br>
                    <div class="form-actions">
                        <button type="submit" class="btn green uppercase">Login</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
                <ul class="sharing">
                    <li><a href="#" class="facebook" title="Facebook"><i class="fa fa-boxed fa-fw fa-facebook"></i> Share on Facebook</a></li>
                    <li><a href="#" class="twitter" title="Twitter"><i class="fa fa-boxed fa-fw fa-twitter"></i> Tweet</a></li>
                    <li><a href="#" class="google" title="Google"><i class="fa fa-boxed fa-fw fa-google-plus"></i> Share on Google+</a></li>
                    <li><a href="#" class="linkedin" title="Linkedin"><i class="fa fa-boxed fa-fw fa-linkedin"></i> Share on LinkedIn</a></li>
                    <li><a href="#" class="mail" title="Email"><i class="fa fa-boxed fa-fw fa-envelope-o"></i> E-mail</a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-4 col_2">
            <h4>About Us</h4>
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
        </div>
        <div class="col-md-2 col_2">
            <h4>Help & Support</h4>
            <ul class="footer_links">
                <li><a href="#">24x7 Live help</a></li>
                <li><a href="contact.html">Contact us</a></li>
                <li><a href="#">Feedback</a></li>
                <li><a href="faq.html">FAQs</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Quick Links</h4>
            <ul class="footer_links">
                <li><a href="privacy.html">Privacy Policy</a></li>
                <li><a href="terms.html">Terms and Conditions</a></li>
                <li><a href="services.html">Services</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Social</h4>
            <ul class="footer_social">
                <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
            </ul>
        </div>
        <div class="clearfix"> </div>
        <div class="copy">
            <p>Copyright © 2015 Marital . All Rights Reserved  | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('form').on('submit', function (e) {
        var focusSet = false;
        if (!$('#email').val()) {
            if ($("#email").parent().next(".validation").length == 0) // only add if not added
            {
                $("#email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter email address !</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#email').focus();
            focusSet = false;
        } else {
            $("#email").parent().next(".validation").remove(); // remove it
        }
        if (!$('#password').val()) {
            if ($("#password").parent().next(".validation").length == 0) // only add if not added
            {
                $("#password").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'> Please enter password !</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            if (!focusSet) {
                $("#password").focus();
            }
        } else {
            $("#password").parent().next(".validation").remove(); // remove it
        }
    });
</script>
<script>
    setTimeout(function() {            
            $('#flashMessage').fadeOut('slow');
            }, 5000);
</script>
    
            