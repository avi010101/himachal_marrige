<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User'); ?>
<style type="text/css">
    .form-required{
        color: #C32143;
    }
</style>


<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            
            <ul>
                <a href="/himachal_marrige/homes/index"><i class="fa fa-home home_1"></i></a> <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">Education and Profession Details</li>
            </ul>
        </div>
        <div class="services">
            <div class="col-md-8 col-sm-12 login_left">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation"  ><a href="#" id="" role="tab" data-toggle="tab" aria-expanded="true" > USERS INFO </a> </li>
                        <li role="presentation"><a href="#" id="" role="tab" data-toggle="tab" aria-expanded="true" >BASIC</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >LOCATION</a> </li>
                        <li role="presentation" class="active"><a href="" role="tab" id="" data-toggle="tab" aria-expanded="false" >EDUCATION CAREER</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >Partner Preferene</a> </li>
                    </ul>
                    <label for="edit-name">(Fields having * are required)<span class="form-required" title="This field is required."></span></label><br><br>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="" aria-labelledby="profile-tab">
                            <form method="post" action="../users/editedu">
                                <h3>Education and Profession</h3>
                                <hr>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input type="hidden" name="user_id" value="<?php echo AuthComponent::user('id'); ?>">
                                    <div class="form-group">

                                        <label for="edit-name">Education Type<span class="form-required" title="This field is required.">*</span></label>

                                        <select id="sub_groups" name="education" value=""  class="form-control required" required ="true">
                                            <option value="<?php echo $educ['Educareer']['education']; ?>"><?php echo $educ['Educareer']['education']; ?></option>
                                            <?php
                                            $edulevel = array("1" => "Bachelors", "Masters", "Doctorate", "Diploma", "Undergraduate", "Associates Degree", "Honours degree", "Trade School", "High School", "Less than high school");
                                            print_r($edulevel);

                                            foreach ($edulevel as $val) {
                                                ?>
                                                <option value="<?php echo $val; ?>">
                                                    <?php echo $val; ?></option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                    </div>
                                    <div class="form-group">

                                        <label for="edit-name">Education Description<span class="form-required" title="This field is required.">*</span></label>

                                        <input type="text" id="fullname" name="edu_detail" value="<?php echo $educ['Educareer']['edu_detail']; ?>" size="60" maxlength="60" class="form-text required" required="true" >
                                    </div>
                                    <div class="form-group">

                                        <label for="edit-name">Occupaton<span class="form-required" title="This field is required.">*</span></label>

                                        <select id="sub_groups" name="occupation" value=""  class="form-control required" required="true">
                                            <option value="<?php echo $educ['Educareer']['occupation']; ?>"><?php echo $educ['Educareer']['occupation']; ?></option>
                                            <option>Govt job</option>
                                            <option>Doctor</option>
                                            <option>Engineer</option>
                                            <option>IT Professional</option>
                                            <option>Defence</option>
                                            <option>Management</option>
                                            <option>Buisness</option>
                                            <option>Teacher</option>
                                            <option>None</option>


                                        </select>
                                    </div>
                                    <div class="form-group">

                                        <label for="edit-name">Annual Salary<span class="form-required" title="This field is required.">*</span></label>

                                        <select id="sub_groups" name="salary" value=""  class="form-control required" required ="true">
                                            <option value="<?php echo $educ['Educareer']['salary']; ?>"><?php echo $educ['Educareer']['salary']; ?></option>
                                            <option>50,000 to 1lakh</option>
                                            <option>1 lakh to 3 lakh</option>
                                            <option>3 lakh to 5 lakh</option>
                                            <option>5 lakh to 7 lakh</option>
                                            <option>7 lakh to 10 lakh </option>
                                            <option>above to 10 lakh</option>



                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="actionBar">  <button type="submit" id="btnsubmit" class="btn btn-success uppercase pull-right">Update</button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-4 col_2">
            <h4>About Us</h4>
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
        </div>
        <div class="col-md-2 col_2">
            <h4>Help & Support</h4>
            <ul class="footer_links">
                <li><a href="#">24x7 Live help</a></li>
                <li><a href="contact.html">Contact us</a></li>
                <li><a href="#">Feedback</a></li>
                <li><a href="faq.html">FAQs</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Quick Links</h4>
            <ul class="footer_links">
                <li><a href="privacy.html">Privacy Policy</a></li>
                <li><a href="terms.html">Terms and Conditions</a></li>
                <li><a href="services.html">Services</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Social</h4>
            <ul class="footer_social">
                <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
            </ul>
        </div>
        <div class="clearfix"> </div>
        <div class="copy">
            <p>Copyright © 2015 Marital . All Rights Reserved   </p>
        </div>
    </div>
</div>
<script>
    $('#btnNext').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $('#btnPrevious').click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
</script>
