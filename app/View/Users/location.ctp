 <?php echo $this->Session->flash('auth'); ?>
 <?php echo $this->Form->create('User'); ?>
 <style type="text/css">
 .form-required{
  color: #C32143;
}
#flashMessage.success {
        
        background: #C32143 url("../images/error.png") no-repeat;        
        color: #FFFFFF;
        padding:5px 25px;
        width: 555px;
    }
</style>


<div class="grid_3">
  <div class="container">
    <div class="breadcrumb1">
        <div id='flashMessages'>
            <?php echo $this->Flash->render() ?> 

        </div>
      <ul>
        <a href="register"><i class="fa fa-home home_1"></i></a> <span class="divider">&nbsp;|&nbsp;</span>
        <li class="current-page">Location Details</li>
      </ul>
    </div>
    <div class="services">
      <div class="col-md-8 col-sm-12 login_left">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation"  ><a href="#" id="" role="tab" data-toggle="tab" aria-expanded="true" > USERS INFO </a> </li>
            <li role="presentation"><a href="#" id="" role="tab" data-toggle="tab" aria-expanded="true" >BASIC</a> </li>
            <li role="presentation" class="active" ><a href="" role="tab" id="" data-toggle="tab" aria-expanded="false" >LOCATION</a> </li>
            <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >EDUCATION CAREER</a> </li>
             <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >Partner Preferene</a> </li>
          </ul>
          <label for="edit-name">(Fields having * are required)<span class="form-required" title="This field is required."></span></label><br><br>
          <div id="myTabContent" class="tab-content">
                 <div role="tabpanel" class="tab-pane" id="" aria-labelledby="profile-tab">
                       <form method="post" action ="../users/location">
                        <h3>Country</h3>
                        <hr>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         
                          <div class="form-group">

                            <label for="edit-name">Country <span class="form-required" title="This field is required.">*</span></label>
                            <select id="edit-name" name="countries" value=""  class="form-control required" required ="ture">
                              <option value="">Select from below</option>
                              <option value="India">India</option>

                              

                            </select>
                          </div>
                        </div>
                        <h3>States</h3>
                        <hr>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                          <div class="form-group">

                            <label for="edit-name">State <span class="form-required" title="This field is required.">*</span></label>
                            <select id="groups" name="states" value=""  class="form-control required" required="ture">
                              <option value="">Select from below</option>
                         <!--  <option>Himachal</option>
                          <option>Haryana</option>
                          <option>Dehli</option>
                          <option>J&K</option>
                          <option>Punjab</option>
                          <option>Rajsthan</option>
                          <option>Utrakhand</option>
                          <option>UP</option> -->
                          <option value='Himachal'>Himachal</option>
                          <option value='Haryana'>Haryana</option>
                          <option value='Dehli'>Dehli</option>
                          <option value='J&K'>J&K</option>
                          <option value='Punjab'>Punjab</option>
                          <option value='Rajsthan'>Rajsthan</option>
                          <option value='Utrakhand'>Utrakhand</option>
                          <option value='Up'>UP</option> -->
                          

                        </select>
                      </div>
                    </div>
                    <h3>City</h3>
                    <hr>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                      <div class="form-group">

                        <label for="edit-name">City <span class="form-required" title="This field is required.">*</span></label>
                        
                        <select id="sub_groups" name="cities" value=""  class="form-control required" required="true">
                          <option value="">Select from below</option>
                          <option value='Himachal'>himachal</option>
                          <option value='Haryana'>Haryana</option>
                          <option value='Dehli'>Dehli</option>
                          <option value='J&K'>J&K</option>
                          <option value='Punjab'>Punjab</option>
                          <option value='Rajsthan'>Rajsthan</option>
                          <option value='Utrakhand'>Utrakhand</option>
                          <option value='Up'>Up</option>
                          <?php
                          
                          foreach($list as $val)

                          {
                            ?>
                            <option value="<?php echo $val; ?>">
                              <?php echo $val; ?></option>
                              <?php
                            }
                            ?>                       

                          </select>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="actionBar">   <a href="../users/basic_info" class="btnPrevious  btn btn-primary">Previous</a> <button type="submit" id="btnsubmit" class="btn btn-success uppercase pull-right">Submit</button></div>
                    </form>
                  </div>
                  
              </div>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="container">
        <div class="col-md-4 col_2">
          <h4>About Us</h4>
          <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
        </div>
        <div class="col-md-2 col_2">
          <h4>Help & Support</h4>
          <ul class="footer_links">
            <li><a href="#">24x7 Live help</a></li>
            <li><a href="contact.html">Contact us</a></li>
            <li><a href="#">Feedback</a></li>
            <li><a href="faq.html">FAQs</a></li>
          </ul>
        </div>
        <div class="col-md-2 col_2">
          <h4>Quick Links</h4>
          <ul class="footer_links">
            <li><a href="privacy.html">Privacy Policy</a></li>
            <li><a href="terms.html">Terms and Conditions</a></li>
            <li><a href="services.html">Services</a></li>
          </ul>
        </div>
        <div class="col-md-2 col_2">
          <h4>Social</h4>
          <ul class="footer_social">
            <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
            <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
            <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
            <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
          </ul>
        </div>
        <div class="clearfix"> </div>
        <div class="copy">
          <p>Copyright © 2015 Marital . All Rights Reserved   </p>
        </div>
      </div>
    </div>
    <script>
    $('#btnNext').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $('#btnPrevious').click(function(){
      $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
    </script>
    <script type="text/javascript">
    
 //  function Submit(){

 // var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;

 // var formemail = $("#email").val();
 // var formreemail = $("#verify_email").val();
 // var name = $("#fullname").val();
 
 // var femail = $("#email").val();
 // var reemail = $("#verify_email").val();
 // var pass = $("#password").val();
 // var mob = $("#mobile").val();
 
 
 //  if($("#fullname").val() == "" ){
 //   $("#fullname").focus();
 //   $("#errorBox").html("Validation Error: enter the First Name");
 //   return false;
 //  }else if($("#email").val() == "" ){
 //    $("#email").focus();
 //    $("#errorBox").html("Validation Error: enter the email");
 //    return false;
 //  }else if(!emailRegex.test(formemail)){
 //    $("#email").focus();
 //    $("#errorBox").html("Validation Error: enter the valid email");
 //    return false;
 //  }else if($('#verify_email').val() == ""){
 //    $("#verify_email").focus();
 //    $("#errorBox").html("Validation Error: re enter the  email");
 //    return false;
 //  }else if(!emailRegex.test(formreemail)){
 //    $("#verify_email").focus();
 //    $("#errorBox").html("Validation Error: re enter the valid email");
 //    return false;
 //  }else if(reemail != femail){
 //    $("#verify_email").focus();
 //    $("#errorBox").html("Validation Error: emails are not matching, re-enter again");
 //    return false;
 //  }else if($('#dob').val() == ""){
 //    $("#dob").focus();
 //    $("#errorBox").html("Validation Error: Enter mobile");
 //    return false;
 //  }else if($('input[name = sex]:checked').length<=0){
 //    $("#sex").focus();
 //    $("#errorBox").html("Validation Error: Select gender");
 //    return false;
 //   }else if($('#mobile').val() == ""){
 //    $("#mobile").focus();
 //    $("#errorBox").html("Validation Error: Enter mobile");
 //    return false;
 //  }else if($('#password').val() == ""){
 //    $("#password").focus();
 //    $("#errorBox").html("Validation Error: Enter the password");
 //    return false;
 //  }
 
 // }
 </script>
 <script type="text/javascript">
 function Submit () {

  var femail = $("#email").val();
  var reemail = $("#verify_email").val();
  var focusSet = false;
  if (!$('#fullname').val()) {
            if ($("#fullname").parent().next(".validation").length == 0) // only add if not added
            {
              $("#fullname").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter fullname</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#fullname').focus();
            focusSet = false;
          } else {
            $("#fullname").parent().next(".validation").remove(); // remove it
          }
          if (!$('#email').val()) {
            if ($("#email").parent().next(".validation").length == 0) // only add if not added
            {
              $("#email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter email address</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#email').focus();
            focusSet = false;
          } else {
            $("#email").parent().next(".validation").remove(); // remove it
          }
          if (!$('#verify_email').val()) {
            if ($("#verify_email").parent().next(".validation").length == 0) // only add if not added
            {
              $("#verify_email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please re-enter email address</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#verify_email').focus();
            focusSet = false;
          } else {
            $("#verify_email").parent().next(".validation").remove(); // remove it
          }
          if(reemail != femail){
            $("#verify_email").focus();
            $("#verify_email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please re-enter email address</div>");
            
          }
          if (!$('#dob').val()) {
            if ($("#dob").parent().next(".validation").length == 0) // only add if not added
            {
              $("#dob").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter date of birth</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#dob').focus();
            focusSet = false;
          } else {
            $("#dob").parent().next(".validation").remove(); // remove it
          }
          if (!$('#mobile').val()) {
            if ($("#mobile").parent().next(".validation").length == 0) // only add if not added
            {
              $("#mobile").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter Mobile Number</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#mobile').focus();
            focusSet = false;
          } else {
            $("#mobile").parent().next(".validation").remove(); // remove it
          }
          if (!$('#password').val()) {
            if ($("#password").parent().next(".validation").length == 0) // only add if not added
            {
              $("#password").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter password</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#password').focus();
            focusSet = false;
          } else {
            $("#password").parent().next(".validation").remove(); // remove it
          }
          ('#tab_content2').show();
          
        }
        </script>
        <script type="text/javascript">
        $("btnsubmit").click(function() {
        $(".services").tabs("select", this.hash);
        });

        </script>
<script>
    setTimeout(function() {
            //alert('hiii');
            $('#flashMessage').fadeOut('slow');
            }, 5000);
</script>