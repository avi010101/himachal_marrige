<?php echo $this->Session->flash('auth'); ?>
<style type="text/css">
    .form-required{ 
        color: #C32143;
    }
    #flashMessage.success {
        
        background: #C32143 url("../images/error.png") no-repeat;        
        color: #FFFFFF;
        padding:5px 25px;
        width: 555px;
    }
</style>
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
         <div id='flashMessages'>
            <?php echo $this->Flash->render() ?> 

        </div>
            <ul>
                <a href="../homes/index"><i class="fa fa-home home_1"></i></a>
                
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><h3 style="color:#C32143;">Welcome: <?php echo AuthComponent:: user('fullname'); ?></h3></li>
            </ul>
        </div>
        <div class="profile">
            <div class="col-md-8 profile_left">
                <div class="col_3">
                    <div class="col-sm-4 row_2">
                        <?php foreach ($imgg as $im) { ?>
                            <a class="fancybox" href="/himachal_marrige/images/<?php echo $im['Image']['filename']; ?>">
                                <img  width="180" height="200"src="/himachal_marrige/images/<?php echo $im['Image']['filename']; ?>" alt=""> 
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-sm-8 row_1">
                        <table class="table_working_hours">
                            <tbody>
                                <?php
                                foreach ($user as $us) {
                                    if (isset($us)) {
                                        ?>
                                        <tr class="opened_1">
                                            <td class="day_label">Name :</td>
                                            <td class="day_value"><?php echo $us['User']['fullname']; ?></td>
                                        </tr>
                                        <tr class="opened_1">
                                            <td class="day_label">Mob No :</td>
                                            <td class="day_value"><?php echo $us['User']['mobile']; ?></td>
                                        </tr>
                                        <tr class="opened_1">
                                            <td class="day_label">Height :</td>
                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['height']; ?></td>
                                        </tr>

                                        <tr class="opened">
                                            <td class="day_label">Religion :</td>
                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['religion']; ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label">Marital Status :</td>
                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['marrital_status']; ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label">Location :</td>
                                            <td class="day_value">India</td>
                                        </tr>

                                        <tr class="closed">
                                            <td class="day_label">Education :</td>
                                            <td class="day_value closed"><span><?php echo $us['Educareer'][0]['edu_detail']; ?></span></td>
                                        </tr>
                                        <tr class="closed">
                                            <td class="day_label">Occupation :</td>
                                            <td class="day_value closed"><span><?php echo $us['Educareer'][0]['occupation']; ?></span></td>
                                        </tr>
                                        <?php
                                    }
                                    break;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"> </div>
                    <?php
                    echo $this->Form->create('Image', array('method' => 'post', 'style' => 'margin-left:0px;margin-top:20px;', 'type' => 'file', 'url' => array('controller' => 'users', 'action' => 'imageupload')));
                    echo $this->Form->file('filename', array("label" => false, "type" => "file", 'placeholder' => 'filename', 'style' => 'margin-bottom:-25px;margin-left:71px;'));
                    ?>

                    <button type="submit" class="btn green" style="background-color:#C32143;color:white;">submit</button>
                    <?php echo $this->Form->end(); ?> 
                </div>
                <div class="col_4">
                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">

                                <div class="basic_1" >
                                    <h3>Basics & Lifestyle</h3>
                                    <div class="col-md-6 basic_1-left">
                                        <table class="table_working_hours">
                                            <tbody>
                                                <?php
                                                foreach ($user as $us) {
                                                    if (isset($us)) {
                                                        ?>
                                                        <tr class="opened_1">
                                                            <td class="day_label">Name :</td>
                                                            <td class="day_value"><?php echo $us['User']['fullname']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Marital Status :</td>
                                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['marrital_status']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Body Type :</td>
                                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['body_type']; ?></td>
                                                        </tr>

                                                        <tr class="opened">
                                                            <td class="day_label">Height :</td>
                                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['height']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Drink :</td>
                                                            <td class="day_value closed"><span><?php echo $us['Basicinfo'][0]['drink']; ?></span></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    break;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 basic_1-left">
                                        <table class="table_working_hours">
                                            <tbody>
                                                <?php
                                                foreach ($user as $us) {
                                                    if (isset($us)) {
                                                        ?>
                                                        <tr class="opened_1">
                                                            <td class="day_label">Community :</td>
                                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['community']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Mother Tongue :</td>
                                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['mother_tounge']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Complexion :</td>
                                                            <td class="day_value"><?php echo $us['Basicinfo'][0]['complexian']; ?></td>
                                                        </tr>


                                                        <tr class="closed">
                                                            <td class="day_label">Diet :</td>
                                                            <td class="day_value closed"><span><?php echo $us['Basicinfo'][0]['diet']; ?></span></td>
                                                        </tr>
                                                        <tr class="closed">
                                                            <td class="day_label">Smoke :</td>
                                                            <td class="day_value closed"><span><?php echo $us['Basicinfo'][0]['smoke']; ?></span></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    break;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"> </div>

                                    <button type="submit" onclick="window.location.href = '../users/editbasic/<?php echo AuthComponent:: user('id'); ?>'" class="btn green" style="float:left;color:white;background-color:#C32143;">Edit</button>
                                </div>
                                <h3>Partner Preference</h3>
                                <div class="col-md-6 basic_1-left">
                                    <table class="table_working_hours">
                                        <tbody>
                                            <?php
                                            foreach ($user as $us) {
                                                if ($us['Partner']) {
                                                    ?>
                                                    <tr class="opened_1">
                                                        <td class="day_label">Age :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['age']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Marital Status :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['marrital_status']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Body Type :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['body_type']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Manglik :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['manglik']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Height :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['height']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Drink :</td>
                                                        <td class="day_value closed"><span><?php echo $us['Partner'][0]['drink']; ?></span></td>
                                                    </tr>
                                                    <?php
                                                }
                                                break;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6 basic_1-left">
                                    <table class="table_working_hours">
                                        <tbody>
                                            <?php
                                            foreach ($user as $us) {
                                                if ($us['Partner']) {
                                                    ?>
                                                    <tr class="opened_1">
                                                        <td class="day_label">Religion :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['religion']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Mother Tongue :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['mother_tounge']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Complexion :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['complexian']; ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label">Community :</td>
                                                        <td class="day_value"><?php echo $us['Partner'][0]['community']; ?></td>
                                                    </tr>
                                                    <tr class="closed">
                                                        <td class="day_label">Diet :</td>
                                                        <td class="day_value closed"><span><?php echo $us['Partner'][0]['diet']; ?></span></td>
                                                    </tr>
                                                    <tr class="closed">
                                                        <td class="day_label">Smoke :</td>
                                                        <td class="day_value closed"><span><?php echo $us['Partner'][0]['smoke']; ?></span></td>
                                                    </tr>
                                                    <?php
                                                }
                                                break;
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="clearfix"> </div>
                                <button type="submit" onclick="window.location.href = '../users/editpartner/<?php echo AuthComponent:: user('id'); ?>'"class="btn green" style="float:left;color:white;background-color:#C32143;">Edit</button>
                                <div class="basic_1">
                                    <h3>Education & Career</h3>
                                    <div class="col-md-6 basic_1-left">
                                        <table class="table_working_hours">
                                            <tbody>
                                                <?php
                                                foreach ($user as $us) {
                                                    if (isset($us)) {
                                                        ?>
                                                        <tr class="opened">
                                                            <td class="day_label">Education   :</td>
                                                            <td class="day_value"><?php echo $us['Educareer'][0]['education']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Education Detail :</td>
                                                            <td class="day_value"><?php echo $us['Educareer'][0]['edu_detail']; ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Occupation Detail :</td>
                                                            <td class="day_value closed"><span><?php echo $us['Educareer'][0]['occupation']; ?></span></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label">Annual Income :</td>
                                                            <td class="day_value closed"><span><?php echo $us['Educareer'][0]['salary']; ?></span></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    break;
                                                }
                                                ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="clearfix"> </div>
                                    <button type="submit" onclick="window.location.href = '../users/editedu/<?php echo AuthComponent:: user('id'); ?>'"class="btn green" style="float:left;color:white;background-color:#C32143;">Edit</button> 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="clearfix"> </div>
    </div>
</div>
</div>
<div class="footer">
    <div class="container">


        <div class="clearfix"> </div>
        <div class="copy">
            <p>Copyright © 2015 Marital . All Rights Reserved  | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
        </div>
    </div>
</div>
<!-- Flex Slider -->
<script defer src="../js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="../css/flexslider.css" type="text/css" media="screen" />
<script>
                                        // Can also be used with $(document).ready()
                                        $(window).load(function () {
                                            $('.flexslider').flexslider({
                                                animation: "slide",
                                                controlNav: "thumbnails"
                                            });
                                        });
</script>   
<script type="text/javascript">
    $(document).ready(function () {
//        $(".fancybox").fancybox({
//            openEffect: 'none',
//            closeEffect: 'none',
//            iframe: {
//                preload: false
//            }
//        });
//        $(".various").fancybox({
//            maxWidth: 800,
//            maxHeight: 600,
//            fitToView: false,
//            width: '70%',
//            height: '70%',
//            autoSize: false,
//            closeClick: false,
//            openEffect: 'none',
//            closeEffect: 'none'
//        });
//        $('.fancybox-media').fancybox({
//            openEffect: 'none',
//            closeEffect: 'none',
//            helpers: {
//                media: {}
//            }
//        });
        setTimeout(function() {
            //alert('hiii');
            $('#flashMessage').fadeOut('slow');
            }, 5000); 
    });
</script>
