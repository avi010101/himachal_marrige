<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User'); ?>
<style type="text/css">
    .form-required{ 
        color: #C32143;
    }
    #flashMessage.error {

        background: #C32143 url("../images/error.png") no-repeat;
        border-color: black;
        color: #FFFFFF;
        padding:5px 25px;
        width: 555px;
    }
</style>


<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="/himachal_marrige/homes/index"><i class="fa fa-home home_1"></i></a> <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">Basic information of User</li>
            </ul>
        </div>
       <div id='flashMessages'>
            <?php echo $this->Flash->render() ?> 

        </div>
        <div class="services">
            <div class="col-md-8 col-sm-12 login_left">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation"  ><a href="#" id="" role="tab" data-toggle="tab" aria-expanded="true" > USERS INFO </a> </li>
                        <li role="presentation" class="active"><a href="" id="" role="tab" data-toggle="tab" aria-expanded="true" >BASIC</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >LOCATION</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >EDUCATION CAREER</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >Partner Preferene</a> </li>
                    </ul>
                    <label for="edit-name">(Fields having * are required)<span class="form-required" title="This field is required."></span></label><br><br>

                    <div role="tabpanel" class="tab-pane" id="" aria-labelledby="profile-tab">
                        <form action = "../users/editbasic/" method ="post">
                            <h3>Marrital Status</h3>
                            <hr>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="hidden" name="user_id" value="<?php echo AuthComponent::user('id'); ?>">

                                <div class="form-group">

                                    <label for="edit-name">Marrital Status <span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="marrital_status" selected ="<?php echo $basicinfo['Basicinfo']['marrital_status']; ?>" class="form-control required" required="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['marrital_status']; ?>"selected ="<?php echo $basicinfo['Basicinfo']['marrital_status']; ?>"><?php echo $basicinfo['Basicinfo']['marrital_status']; ?></option>
                                        <option value="Never Married">Never Married</option>
                                        <option value="Married" >Married</option>
                                        <option value="Divorcy" >Divorcy</option>
                                        <option value="Widow"  >Widow</option>

                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="edit-name">Children<span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="children" value=""  class="form-control required" required =   "true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['children']; ?>"><?php echo $basicinfo['Basicinfo']['children']; ?></option>
                                        <option value="None">None</option>
                                        <option value="One">One</option>
                                        <option value="Two">Two</option>
                                        <option value="Three">Three</option>
                                        <option value="Four">Four</option>
                                        <option value="Five">Five</option>
                                    </select>
                                </div>

                            </div>
                            <h3>Physical Status</h3>
                            <hr>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group">

                                    <label for="edit-name">Height <span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="height" value=""  class="form-control required" required ="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['height']; ?>"><?php echo $basicinfo['Basicinfo']['height']; ?></option>
                                        <?php
                                        $height = array("1" => "4ft 5in - 134cm", "4ft 6in - 137cm", "4ft 7in - 139cm", "4ft 8in - 142cm", "4ft 9in - 144cm", "4ft 10in - 147cm", "4ft 11in - 149cm", "5ft - 152cm", "5ft 1in - 154cm", "5ft 2in - 157cm", "5ft 3in - 160cm", "5ft 4in - 162cm", "5ft 5in - 165cm", "5ft 6in - 167cm", "5ft 7in - 170cm", "5ft 8in - 172cm", "5ft 9in - 175cm", "5ft 10in - 177cm", "5ft 11in - 180cm", "6ft - 182cm", "6ft 1in - 185cm", "6ft 2in - 187cm", "6ft 3in - 190cm", "6ft 4in - 193cm", "6ft 5in - 195cm", "6ft 6in - 198cm", "6ft 7in - 200cm", "6ft 8in - 203cm", "6ft 9in - 205cm", "6ft 10in - 208cm", "6ft 11in - 210cm", "7ft - 213cm");
                                        //print_r($height);

                                        foreach ($height as $val) {
                                            ?>
                                            <option value="<?php echo $val; ?>">
                                                <?php echo $val; ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="form-group">

                                    <label for="edit-name">Body Type <span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="body_type" value=""  class="form-control" required"true">
                                            <option value="<?php echo $basicinfo['Basicinfo']['body_type']; ?>"><?php echo $basicinfo['Basicinfo']['body_type']; ?></option>
                                        <option value="Slim">Slim</option>
                                        <option value="Average">Average</option>
                                        <option value="Heavy">Heavy</option>
                                    </select>
                                </div>



                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="edit-name">Complexian<span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="complexian" value=""  class="form-control" required"true">
                                            <option value="<?php echo $basicinfo['Basicinfo']['complexian']; ?>"><?php echo $basicinfo['Basicinfo']['complexian']; ?></option>
                                        <option value ="Fair">Fair</option>
                                        <option value ="Very Fair">Very Fair</option>
                                        <option value="Dusky">Dusky</option>
                                        <option value="Dark">Dark</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="edit-name">Disability<span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="disabilty" value=""  class="form-control required">
                                        <option><?php echo $basicinfo['Basicinfo']['disabilty']; ?></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="edit-name">Disability Detail <span class="form-required" title="This field is required."></span></label>
                                    <textarea class="form-control bio" placeholder="" rows="3" name="disability_details" value="<?php echo $basicinfo['Basicinfo']['disability_details']; ?>"></textarea>
                                </div>

                            </div>
                            <h3>Community and Religion</h3>
                            <hr>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group">

                                    <label for="edit-name">Religion <span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="religion" value=""  class="form-control required" required ="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['religion']; ?>"><?php echo $basicinfo['Basicinfo']['religion']; ?></option>
                                        <?php
                                        $religion = array("1" => "Hindu", "Muslim", "Sikh", "Jain", "Christion", "Parsi", "Buddhist", "Jewish");
                                        print_r($religion);

                                        foreach ($religion as $val) {
                                            ?>
                                            <option value="<?php echo $val; ?>">
                                                <?php echo $val; ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="edit-name">Community<span class="form-required" title="This field is required." >*</span></label>
                                    <select id="edit-name" name="community" value=""  class="form-control required" required="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['community']; ?>"><?php echo $basicinfo['Basicinfo']['community']; ?></option>
                                        <option value="Brahmin">Brahmin</option>
                                        <option value="Dhiman">Dhiman</option>
                                        <option value="Lohar">Lohar</option>
                                        <option value="Rajpoot">Rajpoot</option>
                                        <option value="Sood">sood</option>
                                        <option value="agarwal">agarwal</option>


                                    </select>
                                </div>



                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="edit-name">Mother Tounge<span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="mother_tounge" value=""  class="form-control required" required="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['mother_tounge']; ?>"><?php echo $basicinfo['Basicinfo']['mother_tounge']; ?></option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Punjabi">Punjabi</option>
                                        <option value="Pahari">Pahari</option>
                                        <option value="Urdu">Urdu</option>
                                        <option value="Haryanvi">Hariyanvi</option>
                                        <option value="English">English</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="edit-name">Manglik<span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="manglik" value=""  class="form-control required">
                                        <option><?php echo $basicinfo['Basicinfo']['manglik']; ?></option>
                                        <option>Yes</option>
                                        <option>No</option>

                                    </select>
                                </div>

                            </div>
                            <h3>Lifestyle</h3>
                            <hr>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group">

                                    <label for="edit-name">Diet <span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="diet" value=""  class="form-control required" required="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['diet']; ?>"><?php echo $basicinfo['Basicinfo']['diet']; ?></option>
                                        <?php
                                        $diet = array("1" => "Vegetarian", "Non-Vegetarian", "Occasionally Non-Veg", "Eggetarian", "Jain", "Vegan");
                                        print_r($diet);

                                        foreach ($diet as $val) {
                                            ?>
                                            <option value="<?php echo $val; ?>">
                                                <?php echo $val; ?></option>
                                            <?php
                                        }
                                        ?>


                                    </select>
                                </div>
                                <div class="form-group">

                                    <label for="edit-name">Smoke <span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="smoke" value=""  class="form-control required" required="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['smoke']; ?>"><?php echo $basicinfo['Basicinfo']['smoke']; ?></option>
                                        <option value="Never">Never</option>
                                        <option value="Regularly">Regularly</option>
                                        <option value="Occasionally">Occasionally</option>

                                    </select>
                                </div>



                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="edit-name">drink<span class="form-required" title="This field is required.">*</span></label>
                                    <select id="edit-name" name="drink" value=""  class="form-control required" required ="true">
                                        <option value="<?php echo $basicinfo['Basicinfo']['drink']; ?>"><?php echo $basicinfo['Basicinfo']['drink']; ?></option>
                                        <option value="Never">Never</option>
                                        <option value="Regularly">Regularly</option>
                                        <option value="Occasionaly">Occasionally</option>

                                    </select>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="actionBar"> <button type="submit" id="btnsubmit" class="btn btn-success uppercase pull-right">Update</button></div>
                        </form>

                    </div>

                </div>
            </div>
        </div>

        <div class="clearfix"> </div>
    </div>
</div>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-4 col_2">
            <h4>About Us</h4>
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
        </div>
        <div class="col-md-2 col_2">
            <h4>Help & Support</h4>
            <ul class="footer_links">
                <li><a href="#">24x7 Live help</a></li>
                <li><a href="contact.html">Contact us</a></li>
                <li><a href="#">Feedback</a></li>
                <li><a href="faq.html">FAQs</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Quick Links</h4>
            <ul class="footer_links">
                <li><a href="privacy.html">Privacy Policy</a></li>
                <li><a href="terms.html">Terms and Conditions</a></li>
                <li><a href="services.html">Services</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Social</h4>
            <ul class="footer_social">
                <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
            </ul>
        </div>
        <div class="clearfix"> </div>
        <div class="copy">
            <p>Copyright © 2015 Marital . All Rights Reserved   </p>
        </div>
    </div>
</div>
<script>
    $('#btnNext').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $('#btnPrevious').click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
</script>

