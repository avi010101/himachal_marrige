<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User'); ?>
<style type="text/css"> 
    .form-required{
        color: #C32143;
    }
    #flashMessage.error {

        background: #C32143 url("../images/error.png") no-repeat;
        border-color: black;
        color: #FFFFFF;
        padding:5px 25px;
        width: 555px;
    }
</style>


<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="register"><i class="fa fa-home home_1"></i></a> <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">Registration</li>
            </ul>
        </div>
        <div id='flashMessages'>
            <?php echo $this->Flash->render() ?> 

        </div><br><br>
        <div class="services">
            <div class="col-md-8 col-sm-12 login_left">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active" ><a href="#tab_content1" id="" role="tab" data-toggle="tab" aria-expanded="true" > USERS INFO </a> </li>
                        <li role="presentation"><a href="#" id="" role="tab" data-toggle="tab" aria-expanded="true" >BASIC</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >LOCATION</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >EDUCATION CAREER</a> </li>
                        <li role="presentation" ><a href="#" role="tab" id="" data-toggle="tab" aria-expanded="false" >Partner Preferene</a> </li>
                    </ul>
                    <label for="edit-name">(Fields having * are required)<span class="form-required" title="This field is required."></span></label><br><br>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <form action="register" method="post">

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="edit-name">Name <span class="form-required" title="This field is required.">*</span></label>
                                        <input type="text" id="fullname" name="fullname" value="" size="60" maxlength="60" class="form-text required" required="true" >
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-pass">email <span class="form-required" title="This field is required.">*</span></label>
                                        <input type="email" id="email" name="email" size="60" maxlength="128" class="form-text required" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-name">verify email<span class="form-required" title="This field is required.">*</span></label>
                                        <input type="email" id="verify_email" name="verify_email" value="" size="60" maxlength="60" class="form-text required" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Age<span class="form-required" title="This field is required.">*</span></label>
                                        <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" id="date_pick1" data-date-start-date="" style="width:230px;">
                                            <input type="text" name="dob" class="form-control form-text"  required="true" style="width:200px;border:1px solid #DDDDDD;">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" style="margin-right: 223px;"><span class="md-click-circle md-click-animate" style="height: 47px; width: 47px; top: 1.5px; left: 2.25px;"></span><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div> 
                                    </div> 
                                    <div class="form-group">
                                        <label>Sex<span class="form-required" title="This field is required.">*</span> </label>&emsp;
                                        <label for="radio-01" class="label_radio">
                                            <input type="radio" value="male" name="sex" id="sex" checked="" required>
                                            Male </label>
                                        <label for="radio-02" class="label_radio">
                                            <input type="radio" value="female" name="sex" id="sex" required>
                                            Female </label>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="edit-name">mobile <span class="form-required" title="This field is required.">*</span></label>
                                        <input type="text" id="mobile" name="mobile" value="" size="60" maxlength="60" class="form-text required" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-name">password <span class="form-required" title="This field is required.">*</span></label>
                                        <input type="password" id="password" name="password" autocomplete="off" class="form-text required" required>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="actionBar"> <a href="#" class="btn btn-default" >Cancel</a>  <button type="submit" id="btnsubmit" class="btn btn-success uppercase pull-right" onclick="Submit()">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 col-md-offset-1">
                <h2> Social Sign up</h2><hr>
                <ul class="sharing">
                    <li><a href="#" class="facebook" title="Facebook"><i class="fa fa-boxed fa-fw fa-facebook"></i> Share on Facebook</a></li>
                    <li><a href="#" class="twitter" title="Twitter"><i class="fa fa-boxed fa-fw fa-twitter"></i> Tweet</a></li>
                    <li><a href="#" class="google" title="Google"><i class="fa fa-boxed fa-fw fa-google-plus"></i> Share on Google+</a></li>
                    <li><a href="#" class="linkedin" title="Linkedin"><i class="fa fa-boxed fa-fw fa-linkedin"></i> Share on LinkedIn</a></li>
                    <li><a href="#" class="mail" title="Email"><i class="fa fa-boxed fa-fw fa-envelope-o"></i> E-mail</a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-4 col_2">
            <h4>About Us</h4>
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
        </div>
        <div class="col-md-2 col_2">
            <h4>Help & Support</h4>
            <ul class="footer_links">
                <li><a href="#">24x7 Live help</a></li>
                <li><a href="contact.html">Contact us</a></li>
                <li><a href="#">Feedback</a></li>
                <li><a href="faq.html">FAQs</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Quick Links</h4>
            <ul class="footer_links">
                <li><a href="privacy.html">Privacy Policy</a></li>
                <li><a href="terms.html">Terms and Conditions</a></li>
                <li><a href="services.html">Services</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Social</h4>
            <ul class="footer_social">
                <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
            </ul>
        </div>
        <div class="clearfix"> </div>
        <div class="copy">
            <p>Copyright © 2015 Marital . All Rights Reserved   </p>
        </div>
    </div>
</div>
<script>
    $('#btnNext').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $('#btnPrevious').click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
</script>
<script type="text/javascript">

//  function Submit(){

// var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;

// var formemail = $("#email").val();
// var formreemail = $("#verify_email").val();
// var name = $("#fullname").val();

// var femail = $("#email").val();
// var reemail = $("#verify_email").val();
// var pass = $("#password").val();
// var mob = $("#mobile").val();


//  if($("#fullname").val() == "" ){
//   $("#fullname").focus();
//   $("#errorBox").html("Validation Error: enter the First Name");
//   return false;
//  }else if($("#email").val() == "" ){
//    $("#email").focus();
//    $("#errorBox").html("Validation Error: enter the email");
//    return false;
//  }else if(!emailRegex.test(formemail)){
//    $("#email").focus();
//    $("#errorBox").html("Validation Error: enter the valid email");
//    return false;
//  }else if($('#verify_email').val() == ""){
//    $("#verify_email").focus();
//    $("#errorBox").html("Validation Error: re enter the  email");
//    return false;
//  }else if(!emailRegex.test(formreemail)){
//    $("#verify_email").focus();
//    $("#errorBox").html("Validation Error: re enter the valid email");
//    return false;
//  }else if(reemail != femail){
//    $("#verify_email").focus();
//    $("#errorBox").html("Validation Error: emails are not matching, re-enter again");
//    return false;
//  }else if($('#dob').val() == ""){
//    $("#dob").focus();
//    $("#errorBox").html("Validation Error: Enter mobile");
//    return false;
//  }else if($('input[name = sex]:checked').length<=0){
//    $("#sex").focus();
//    $("#errorBox").html("Validation Error: Select gender");
//    return false;
//   }else if($('#mobile').val() == ""){
//    $("#mobile").focus();
//    $("#errorBox").html("Validation Error: Enter mobile");
//    return false;
//  }else if($('#password').val() == ""){
//    $("#password").focus();
//    $("#errorBox").html("Validation Error: Enter the password");
//    return false;
//  }

// }
</script>
<script type="text/javascript">
    function Submit() {

        var femail = $("#email").val();
        var reemail = $("#verify_email").val();
        var focusSet = false;
        if (!$('#fullname').val()) {
            if ($("#fullname").parent().next(".validation").length == 0) // only add if not added
            {
                $("#fullname").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter fullname</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#fullname').focus();
            focusSet = false;
        } else {
            $("#fullname").parent().next(".validation").remove(); // remove it
        }
        if (!$('#email').val()) {
            if ($("#email").parent().next(".validation").length == 0) // only add if not added
            {
                $("#email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter email address</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#email').focus();
            focusSet = false;
        } else {
            $("#email").parent().next(".validation").remove(); // remove it
        }
        if (!$('#verify_email').val()) {
            if ($("#verify_email").parent().next(".validation").length == 0) // only add if not added
            {
                $("#verify_email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please re-enter email address</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#verify_email').focus();
            focusSet = false;
        } else {
            $("#verify_email").parent().next(".validation").remove(); // remove it
        }
        if (reemail != femail) {
            $("#verify_email").focus();
            $("#verify_email").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please re-enter email address</div>");

        }
        if (!$('#dob').val()) {
            if ($("#dob").parent().next(".validation").length == 0) // only add if not added
            {
                $("#dob").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter date of birth</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#dob').focus();
            focusSet = false;
        } else {
            $("#dob").parent().next(".validation").remove(); // remove it
        }
        if (!$('#mobile').val()) {
            if ($("#mobile").parent().next(".validation").length == 0) // only add if not added
            {
                $("#mobile").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter Mobile Number</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#mobile').focus();
            focusSet = false;
        } else {
            $("#mobile").parent().next(".validation").remove(); // remove it
        }
        if (!$('#password').val()) {
            if ($("#password").parent().next(".validation").length == 0) // only add if not added
            {
                $("#password").parent().append("<div class='validation' style='color:#C32143;margin-bottom: 20px;'>Please enter password</div>");
            }
            e.preventDefault(); // prevent form from POST to server
            $('#password').focus();
            focusSet = false;
        } else {
            $("#password").parent().next(".validation").remove(); // remove it
        }
        ('#tab_content2').show();

    }
</script>
<script>
    $(document).ready(function () {

        $("#date_pick1").datepicker({
            defaultDate: new Date(),
            autoclose: 'true'
        });

        $("input[type=text]:first").focus();



    });

</script>
