<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="index.html"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">Regular Search</li>
            </ul>
        </div>      
        <div class="col-md-9 search_left">
            <form action="../users/regular_search" method="post">	
                <div class="form_but1">
                    <label class="col-sm-5 control-lable1" for="sex">Gender : </label>
                    <div class="col-sm-7 form_radios">
                        <input type="radio" class="radio_1" name="sex"value="male"/> male &nbsp;&nbsp;
                        <input type="radio" class="radio_1" name="sex" value="female"  /> female
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="form_but1">
                    <label class="col-sm-5 control-lable1" for="sex">Marital Status : </label>
                    <div class="col-sm-7 form_radios">
                        <select id="edit-name" name="status" value=""  class="form-control required" required="true">
                            <option value="">-- Select--</option>
                            <option value="Never Married">Never Married</option>
                            <option value="Married">Married</option>
                            <option value="Divorcy">Divorcy</option>


                        </select>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <div class="form_but1">
                    <label class="col-sm-5 control-lable1" for="sex">Religion : </label>
                    <div class="col-sm-7 form_radios">

                        <select id="edit-name" name="religion" value=""  class="form-control required" required="true">
                            <option value="">Select from below</option>
                            <?php
                            $religion = array("1" => "Hindu", "Muslim", "Sikh", "Jain", "Christion", "Parsi", "Buddhist", "Jewish");
                            print_r($religion);

                            foreach ($religion as $val) {
                                ?>
                                <option value="<?php echo $val; ?>">
                                    <?php echo $val; ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="form_but1">
                    <label class="col-sm-5 control-lable1" for="sex">Mother Tongue : </label>
                    <div class="col-sm-7 form_radios">
                        <select id="edit-name" name="language" value=""  class="form-control required" required="true">
                            <option value="">Select from below</option>
                            <option value="Hindi">Hindi</option>
                            <option value="Punjabi">Punjabi</option>
                            <option value="Pahari">Pahari</option>
                            <option value="Urdu">Urdu</option>
                            <option value="Haryanvi">Hariyanvi</option>
                            <option value="English">English</option> 
                        </select>

                    </div>
                    <div class="clearfix"> </div>
                </div>              
                <div class="form_but1">
                    <label class="col-sm-5 control-lable1" for="sex">Age : </label>
                    <div class="col-sm-7 form_radios">

                        <input  id ="age" name="age" placeholder = "From:" style="width: 34%;" type="text" required="true">&nbsp;-&nbsp;<input id="age1" name="age1" placeholder="To:" style="width: 34%;" type="text" required="true">


                        <div class="clearfix"> </div>
                        <button type="submit" id="btnsubmit" class="btn btn-success uppercase pull-right" style="margin-top: -28px; background-color: #C32143">find Profile</button>

                    </div>
                    <div class="clearfix"> </div>
                </div>

            </form>
            <div class="paid_people">
                <h1 style="color: #C32143">Matched Profile</h1>
                <div class="row_1">
                    <div class="col-sm-6 paid_people-left">
                        <?php
                        foreach ($match as $mtch) {
                            //debug();
                            //die;  
                            ?>
                            <ul class="profile_item">
                                <a href="../users/view_profile/<?php echo $mtch['User']['id']; ?>">
                                    <li class="profile_item-img">
                                        <img src="../images/<?php echo$mtch['User']['Image'][0]['filename']; ?>" class="img-responsive" alt=""/>
                                    </li>
                                    <li class="profile_item-desc">
                                        <h4>2458741</h4>
                                        <p><?php echo $mtch['User']['fullname']; ?></p>
                                        <h5>View Full Profile</h5>
                                    </li>
                                    <div class="clearfix"> </div>
                                </a>
                            </ul>
                        <?php } ?>
                    </div>

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 match_right">
            <div class="profile_search1">
                <form>
                    <input type="text" class="m_1" name="ne" size="30" required="" placeholder="Enter Profile ID :">
                    <input type="submit" value="Go">
                </form>
            </div>

           
        <div class="clearfix"> </div>
    </div>
</div>
<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3150859.767904157!2d-96.62081048651531!3d39.536794757966845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1408111832978"> </iframe>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-4 col_2">
            <h4>About Us</h4>
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris."</p>
        </div>
        <div class="col-md-2 col_2">
            <h4>Help & Support</h4>
            <ul class="footer_links">
                <li><a href="#">24x7 Live help</a></li>
                <li><a href="contact.html">Contact us</a></li>
                <li><a href="#">Feedback</a></li>
                <li><a href="faq.html">FAQs</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Quick Links</h4>
            <ul class="footer_links">
                <li><a href="privacy.html">Privacy Policy</a></li>
                <li><a href="terms.html">Terms and Conditions</a></li>
                <li><a href="services.html">Services</a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4>Social</h4>
            <ul class="footer_social">
                <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
                <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
            </ul>
        </div>
        <div class="clearfix"> </div>
        <div class="copy">
            <p>Copyright © 2015 Marital . All Rights Reserved  | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
        </div>
    </div>
</div>
<!-- FlexSlider -->
<link href="../css/flexslider.css" rel='stylesheet' type='text/css' />
<script defer src="../js/jquery.flexslider.js"></script>

<!-- FlexSlider -->
</body>
</html>	