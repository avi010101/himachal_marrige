<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('Utitlity', 'Security');

class UsersController extends AppController {

    public $helpers = array('Html', 'Form', 'Flash', 'Session');
    public $components = array('RequestHandler', 'Flash',
        'session',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email', 'password' => 'password')
                )
            )
        ),
    );

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
                'plugin' => 'Users'
            ],
        ]);
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'register', 'logout', 'basic_info', 'view_profile', 'location', 'edu_details', 'partner', 'search', 'regular_search');
    }

    /*     * ******************************************************************************************************************
     * Login Page
     */

    public function login() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Flash->success('You are logged in');
                return $this->redirect($this->Auth->redirectUrl('profile'));
            } else {
                $this->Flash->error('Invailed email & password!');
            }
        }
    }

    /*     * ******************************************************************************************************************
     *  Users logout
     */

    public function logout() {
        $this->Session->destroy('User');
        if ($this->Auth->logout()) {
            $this->redirect("login");
        }
        else{
            $this->Auth->loggedIn();
            return $this->redirect($this->Auth->redirectUrl('profile'));
        }
    }

    /*     * *****************************************************************************************************************************
     * Add Users
     */

    public function register() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            //debug($this->request->data);die;
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $id = $this->User->getLastInsertId();
                $this->Session->write("id", $id);
                return $this->redirect($this->Auth->redirectUrl('basic_info'));
            } else {
                $this->User->validationErrors;
            }
        }
    }

    /*     * ******************************************************************************************************************
     *  Users Basic information
     */

    public function basic_info() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            $result = $this->request->data;
            //debug($result);die;
            $this->loadModel('Basicinfo');
            if ($this->Basicinfo->save($result)) {
                $this->Flash->success('Basic Information saved successfully!');
                return $this->redirect($this->Auth->redirectUrl('location'));
            }
        }
    }

    /*     * **************************************************************************************************************************************
     *  Users Location
     */

    public function location() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            $u_id = $this->Session->read("id");
            $result = $this->request->data;
            $r = $result['countries'];
            $data = array('Countries' => array('countries' => $r, 'user_id' => $u_id));
            $c = $result['states'];
            $d = $result['cities'];
            $this->loadModel('Countries');
            $this->Countries->save($data);
            $id = $this->Countries->getLastInsertId();
            $dta = array('States' => array('states' => $c, 'countries_id' => $id, 'user_id' => $u_id));
            $this->loadModel('States');
            $this->States->save($dta);
            $s_id = $this->States->getLastInsertId();
            $dt = array('Cities' => array('cities' => $d, 'countries_id' => $id, 'states_id' => $s_id, 'user_id' => $u_id));
            $this->loadModel('Cities');
            $this->Cities->save($dt);
            if ($this->Countries->save($data) || $this->States->save($dta) || $this->Cities->save($dt) == true) {
                $this->Flash->success('Location saved successfully!');
                return $this->redirect($this->Auth->redirectUrl('edu_details'));
            }
        }
    }

    /*     * *************************************************************************************************************************
     *  Users Education and Career information
     */

    public function edu_details() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            $result = $this->request->data;
            $this->loadModel('Educareer');
            if ($this->Educareer->save($result)) {
                $this->Flash->success('Education details saved successfully!');
                return $this->redirect($this->Auth->redirectUrl('partner'));
            }
        }
    }

    /*     * *****************************************************************************************************************************
     *  Partner Details
     */

    public function partner() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            $result = $this->request->data;
            $this->loadModel('Partner');
            if ($this->Partner->save($result)) {
                $this->Flash->success('Register Successfully!');
                return $this->redirect($this->Auth->redirectUrl('login'));
            }
        }
    }

    /*     * *************************************************************************************************************************
     *  Users Profile
     */

    public function profile() {
        $this->layout = 'profilelayout';
        $user = AuthComponent:: user('id');
        $this->loadModel('Image');
        $cd = array('Image.user_id' => $user);
        $img = $this->Image->find('all', array('conditions' => $cd));
        //debug($img);die;
        $this->set('imgg', $img);
        $this->loadModel('User');
        $cnd = array('User.id' => $user);
        $usr = $this->User->find('all', array('conditions' => $cnd));
        $this->set('user', $usr);
    }

    /*     * *************************************************************************************************************************
     *  View Profile
     */

    public function view_profile($id) {
        //debug($id);die;
        $this->layout = 'loginlayout';
        $this->loadModel('Image');
        $cd = array('Image.user_id' => $id);
        $img = $this->Image->find('all', array('conditions' => $cd));
        //debug($img);die;
        $this->set('imgg', $img);
        $this->loadModel('User');
        $cnd = array('User.id' => $id);
        $usr = $this->User->find('all', array('conditions' => $cnd));
        $this->set('user', $usr);
    }

    /*     * *************************************************************************************************************************
     *  Users Profile _ pic
     */

    public function imageupload() {

        if ($this->request->is('post')) {
            $this->autoRender = $this->layout = FALSE;
            $profile_pic = $this->request->data['Image']['filename']['name'];
            $profile_pic = str_replace(' ', '_', $profile_pic);
            $this->request->data['Image']['filename']['name'] = $profile_pic;
            $u_id = AuthComponent:: user('id');
            $validate = array(
                'filename' => array(
                    'extension' => array(
                        'rule' => 'checkImg',
                        'message' => 'Upload a image and should not exceed 2MB'
                    ),
                ),
            );

            $this->loadModel('Image');
            $this->Image->set($this->request->data);
            $this->Image->validate = $validate;
            if ($this->Image->validates($this->request->data)) {
                move_uploaded_file($this->request->data['Image']['filename']['tmp_name'], WWW_ROOT . DS . 'images' . DS . $profile_pic);
                $data = array('Image' => array(
                        'user_id' => $u_id,
                        'filename' => $profile_pic,
                        'filepath' => '/' . 'images' . '/' . $profile_pic
                ));
                $this->loadModel('Image');
                if ($this->Image->save($data, false)) {
                    $id = $this->Image->getLastInsertId();
                    $this->Session->write("img", $id);
                    $this->Flash->success('Image Uploaded Successfully!');
                    $this->redirect('profile');
                }
            }
        }
    }

    /*     * *************************************************************************************************************************
     *  edit basicinfo
     */

    public function editbasic($id) {
        $this->layout = 'loginlayout';
        $this->loadModel('Basicinfo');
        $basicinfo = $this->Basicinfo->find('first', array('conditions' => array('user_id' => $id)));
        $cid = $basicinfo['Basicinfo']['id'];
        $this->set(compact('basicinfo'));
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Basicinfo->id = $cid;
            if($this->Basicinfo->save($this->request->data)){
            $this->Flash->success('Data Updated Successfully!');
            $this->redirect(array('action' => 'profile'));
        }
        }
    }

    /*     * *************************************************************************************************************************
     *  edit Edu & Career
     */

    public function editedu($id = NULL) {
        $this->layout = 'loginlayout';
        $this->loadModel('Educareer');
        $educ = $this->Educareer->find('first', array('conditions' => array('user_id' => $id)));
        $uid = $educ['Educareer']['id'];
        $this->set(compact('educ'));
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Educareer->id = $uid;
            if($this->Educareer->save($this->request->data)) {
            $this->Flash->success('Data Updated Successfully!');
            $this->redirect(array('action' => 'profile'));
        }
        }
    }

    /*     * *************************************************************************************************************************
     *  edit Partner Preference
     */

    public function editpartner($id = NULL) {
        $this->layout = 'loginlayout';
        $this->loadModel('Partner');
        $partner = $this->Partner->find('first', array('conditions' => array('user_id' => $id)));
        $iid = $partner['Partner']['id'];
        $this->set(compact('partner'));
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Partner->id = $iid;
            if($this->Partner->save($this->request->data)) {
            $this->Flash->success('Data Updated Successfully!');
            $this->redirect(array('action' => 'profile'));
            }
        }
    }

    /*     * *************************************************************************************************************************
     *  search Profile
     */

    public function search() {
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            $search = $this->request->data;
            //debug($search);die;
            $gender = $search['gender'];
            $age1 = $search['age'];
            $age2 = $search['age1'];
            $stats = $search['status'];
            // $this->loadModel('User');
            $this->loadModel('Basicinfo');
            $cnd = array('Basicinfo.sex ' => $gender, 'Basicinfo.marrital_status' => $stats, 'Basicinfo.age BETWEEN ? AND ?' => array($age1, $age2));
            //debug($cnd);die;
            $match = $this->Basicinfo->find('all', array('conditions' => $cnd, 'recursive' => 2));
            //debug($match);
            //die;
            $this->set('match', $match);
        }
    }

    /*     * *************************************************************************************************************************
     *  search Profile
     */

    public function regular_search() {
        //debug($this->request->data);die;
        $this->layout = 'loginlayout';
        if ($this->request->is('post')) {
            $search = $this->request->data;
            //debug($search);die;
            $gender = $search['sex'];
            $stats = $search['status'];
            $religion = $search['religion'];
            $lang = $search['language'];
            $age1 = $search['age'];
            $age2 = $search['age1'];
            $this->loadModel('Basicinfo');
            $cnd = array('Basicinfo.sex ' => $gender, 'Basicinfo.marrital_status' => $stats, 'Basicinfo.religion ' => $religion, 'Basicinfo.mother_tounge ' => $lang, 'Basicinfo.age BETWEEN ? AND ?' => array($age1, $age2));
            $match = $this->Basicinfo->find('all', array('conditions' => $cnd, 'recursive' => 2));
            //debug($match);
            //die;
            $this->set('match', $match);
        }
    }

    /*     * *************************************************************************************************************************
     *  Users search
     */
}
