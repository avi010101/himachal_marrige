<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/* * *********************************************************************
 * Members Controller
 */

class HomesController extends AppController {

    public $components = array('RequestHandler', 'Flash');
    
    public function beforeFilter() {
        parent::beforeFilter();

        $this->Auth->allow('index', 'login');
    }

    /*     * ******************************************************************
     * Index
     */

    public function index() {
        
        $this->layout = 'indexlayout';
    }

}
