<?php

App::uses('AuthComponent', 'Controller/Component');

class Image extends AppModel {

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),        
    ); 
   
    public function checkImg($data) {
        if (!empty($data['filename'])) {
            $file = $data['filename'];
            if ($file['type'] == 'image/jpeg' || $file['type'] == 'image/png' || $file['type'] == 'image/gif') {
                if ($file['size'] <= 3097152) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

}
