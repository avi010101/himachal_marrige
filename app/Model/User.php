<?php

App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel {

    public $hasMany = array(
        'Basicinfo' => array(
            'className' => 'Basicinfo',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Educareer' => array(
            'className' => 'Educareer',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Partner' => array(
            'className' => 'Partner',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Image' => array(
            'className' => 'Image',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    public $validate = array(
        'fullname' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Fullname  cannot  be Empty',
                'allowEmpty' => false,
            ),
            'between' => array(
                'rule' => array('between', 1, 20),
                'required' => true,
                'message' => 'names must be between 1 to 20 characters'
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('email', true),
                'message' => 'Please provide a valid email address.',
                'allowEmpty' => false,
            ),
            'unique' => array(
                'rule' => array('isUniqueEmail'),
                'message' => 'This email is already in use',
            ),
            'between' => array(
                'rule' => array('between', 6, 60),
                'message' => 'email must be between 6 to 60 characters'
            )
        ),
        'verify_email' => array(
            'required' => array(
                'rule' => array('email', true),
                'message' => 'Please provide a valid email address.',
                'allowEmpty' => false,
            ),
            'equaltofield' => array(
                'rule' => array('equaltofield', 'email'),
                'message' => 'Both emails must match.'
            )
        ),
        'mobile' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A mobile no is required',
                'allowEmpty' => false,
            ),
            'max_length' => array(
                'rule' => array('maxLength', '11'),
                'message' => 'Mobile no.must have a mimimum of 11 characters'
            ),
            'min_length' => array(
                'rule' => array('minLength', '10'),
                'message' => 'Mobile no.must have a mimimum of 11 characters'
            )
        ),
    );

    //public $useDbConfig = 'local';
    function isUniqueEmail($check) {

        $email = $this->find(
                'first', array(
            'fields' => array(
                'User.id'
            ),
            'conditions' => array(
                'User.email' => $check['email']
            )
                )
        );

        if (!empty($email)) {
            if ($this->data[$this->alias]['id'] == $email['User']['id']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function equaltofield($check, $otherfield) {
        //get name of field 
        $fname = '';
        foreach ($check as $key => $value) {
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
    }

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        $this->data['User']['resetkey'] = Security::hash(mt_rand(), 'md5', true);
        return true;
    }

}
